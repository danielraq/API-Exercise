CREATE TYPE gender as ENUM ('male', 'female');

CREATE TABLE people(
  survived bool,
  pclass smallint,
  name varchar(100),
  sex gender,
  age float8,
  siblings_spouses_aboard int,
  parents_children_aboard int,
  fare float8
);

ALTER TABLE people ADD COLUMN id SERIAL PRIMARY KEY;