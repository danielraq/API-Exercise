from flask import Flask
from flask_restful import Api
import api_resource
import db_setup

application = Flask(__name__)
api = Api(application)
db_setup.setup_db()

@application.route("/people")
def get_people():
    return api_resource.get_people()


@application.route("/updatestatus/id/<id>/survived/<status>")
def update_status(id, status):
    return api_resource.update_status(id, status)


@application.route("/delete/<id>")
def delete_people(id):
    return api_resource.delete_people(id)


@application.route("/insert/survived/<status>/pclass/<pclass>/fname/<fname>/sname/" +
                   "<sname>/sex/<sex>/age/<age>/ssa/<ssa>/pca/<pca>/fare/<fare>")
def insert_people(status, pclass, fname, sname, sex, age, ssa, pca, fare):
    return api_resource.insert_people(status, pclass, fname, sname, sex, age, ssa, pca, fare)


if __name__ == '__main__':
    application.run()
