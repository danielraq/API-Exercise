#!/bin/sh

export dbname='db'
export dbuser='user'
export dbpass='topsecret'
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
S2I_SCRIPT=${SCRIPT_PATH}/s2i

downloads2i(){
    if [ ! -f "${S2I_SCRIPT}" ]; then
        wget https://github.com/openshift/source-to-image/releases/download/v1.1.10/source-to-image-v1.1.10-27f0729d-linux-amd64.tar.gz -P ${SCRIPT_PATH}
        tar -zxvf ${SCRIPT_PATH}/source-to-image-v1.1.10-27f0729d-linux-amd64.tar.gz -C ${SCRIPT_PATH}/
        rm ${SCRIPT_PATH}/source-to-image-v1.1.10-27f0729d-linux-amd64.tar.gz
    fi
}


buildapp(){
    ${SCRIPT_PATH}/s2i build git@gitlab.com:danielraq/API-Exercise.git centos/python-35-centos7 titanicapp

}

rundb(){
    docker rm -f titanic_db 2> /dev/null
    docker run --name titanic_db  -d  -e POSTGRESQL_USER=${dbuser} -e POSTGRESQL_PASSWORD=${dbpass} \
        -e POSTGRESQL_DATABASE=${dbname}  -p 5432:5432 centos/postgresql-96-centos7
    }

run(){
    #at first run application will insert data
    docker run -d -p 8080:8080 -e DBNAME=${dbname} -e DBUSER=${dbuser} -e DBPASS=${dbpass}  -e DBHOST=`hostname` titanicapp
}

downloads2i
rundb
buildapp
run



