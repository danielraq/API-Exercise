# Local instance

## Requirements
 1. Applications:  
    * Docker 
    * GIT
    * wget
 2. SSH key which will allow to dowload from git@gitlab.com:danielraq/API-Exercise.git 
 

## How to build
```buildoutcfg
/bin/sh start.sh
```
 Application will be available on localhost:8080 
