# API-exercise - solution

Below you will find solution for the Container Solutions technical assessment task.  

## Stucture 

* [./](./): Appliocation code
* [./openshift](./openshift): Openshift template and example config file 
* [./local](./local): Script to build and test application without Kubernetes 

## Solution details 
 I decided to create an empty database without any data or structure. It will be injected by the application when it will noticed that it is connecting to empty database. Database application is connected to a volume so data will not vanish when container will restart.
 The whole CD process is done by GitLab and OpenShift. Unfortunately application the application container build process has to be triggered manually because of network restrictions.
 
## API
* reading  - [ROUTE_LINK]/people
* writing  - [ROUTE_LINK]/insert/survived/<status\>/pclass/<pclass\>/fname/<fname\>/sname/<sname\>/sex/<sex\>/age/<age\>/ssa/<ssa\>/pca/<pca\>/fare/<fare\>'
* deleting - [ROUTE_LINK]/delete/<id>
* updating - [ROUTE_LINK]/updatestatus/id/<id>/survived/<status>

### What could be improved
 * I used OpenShift [build](https://docs.openshift.com/container-platform/3.9/dev_guide/builds/index.html) feature, if GitLab could send webhooks to my OS machine i could create automatic build/deployment proces triggered by git commit.
 * Data are not consistent eg. sometimes age is int and sometimes float
 * Supposedly, if  load of application will be to big for one container, autoscaling should be added      