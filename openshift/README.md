# OPENSHIFT

## Requirements
 1. Applications:  
    * OpenShift/[MiniShift](https://docs.okd.io/latest/minishift/getting-started/index.html) 
    * [OpenShift CLI](https://docs.openshift.com/container-platform/3.9/cli_reference/get_started_cli.html)
 2. OpenShift:
    * SSHkey in secret to clone from git@gitlab.com:danielraq/API-Exercise.git 
      ```oc secrets new sshsecret ssh-privatekey=$HOME/.ssh/id_rsa```
    * Database password secret (you can use example file: example_pass_sec.yaml)
      ```oc create -f example_pass_sec.yaml``` 
 3. Enviroment parameters set in parameters file (you can use example file: example_parameters.env)

## How to build
```buildoutcfg
oc process -f ./titanic_template.yaml --param-file=./example_parameters.env |./oc create -f -
```
