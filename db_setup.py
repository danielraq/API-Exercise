import db_connect
import csv
import logging

db_connect = db_connect.get_db_connect()
db_script = './db_setup/init.sql'
db_data_script = './db_setup/titanic.csv'
logging.basicConfig(level=logging.DEBUG)


def setup_table():
    sql = open(db_script, 'r')
    if not db_connect.dialect.has_table(db_connect, 'people'):
        db_connect.execute(sql.read())
        logging.info('Table \'people\' created.')


def insert_data():
    count = db_connect.execute('select count(*) from people')
    if count.fetchone()[0] == 0:
        reader = csv.reader(open(db_data_script, 'r'))
        for row in reader:
            db_connect.execute(
                "insert into people (survived,pclass,name,sex,age,siblings_spouses_aboard,parents_children_aboard, fare) "
                "values (%s, %s, \'%s\', \'%s\', %s, %s, %s, %s)" % (
                    bool(row[0]), row[1], row[2], row[3], row[4], row[5], row[6], row[7]))
        logging.info('Data inserted to \'people\' table')


def setup_db():
    setup_table()
    insert_data()
