import db_connect
import json
from flask_restful import Resource

db_connect = db_connect.get_db_connect()


def get_people():
    conn = db_connect.connect()
    query = conn.execute(
        "select id, survived,pclass,name,sex,age,siblings_spouses_aboard,parents_children_aboard, fare from people")
    res = [dict(zip(tuple(query.keys()), i)) for i in query.cursor]
    return json.dumps(res)


def update_status(id, status):
    if int(status) == 1:
        new_status = 'TRUE'
    elif int(status) == 0:
        new_status = 'FALSE'
    else:
        res = [{'message': 'wrong status'}]
        return json.dumps(res)

    conn = db_connect.connect()
    conn.execute("update people set survived = %s where id = %d" % (new_status, int(id)))
    res = [{'message': 'done'}]
    return json.dumps(res)


def delete_people(id):
    conn = db_connect.connect()
    conn.execute("delete from people where id = %d" % (int(id)))
    res = [{'message': 'done'}]
    return json.dumps(res)


def insert_people(status, pclass, fname, sname, sex, age, ssa, pca, fare):
    if int(status) == 1:
        new_status = 'TRUE'
    elif int(status) == 0:
        new_status = 'FALSE'
    else:
        res = [{'message': 'wrong status'}]
        return json.dumps(res)

    full_name = fname + " " + sname
    conn = db_connect.connect()
    conn.execute(
        "insert into people (survived,pclass,name,sex,age,siblings_spouses_aboard,parents_children_aboard, fare) "
        "values (%s, %s, \'%s\', \'%s\', %s, %s, %s, %s)" %
        (new_status, pclass, full_name, sex, age, ssa, pca, fare))
    res = [{'message': 'done'}]
    return json.dumps(res)
